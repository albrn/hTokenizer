{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLists #-}
module Main where

import Arguments (Parameters(..), WordSets)
import qualified Arguments (parse)
import Chunk (Chunk(..), TextChunk(..))
import qualified Chunk (stream, toString)
import Zipper (Zipper(..), zipDown)
import qualified Zipper (back, forth, insert)
import Control.Monad.Reader (Reader, runReader, asks)
import Data.Monoid ((<>))
import Data.Text (Text)
import qualified Data.Text as Text (pack, unpack)
import XML.Filter (Node(..), Occurrence(..), Result(..), Strategy(..), apply, printNode)

type Chunks = Zipper Chunk

maxQueueSize :: Int
maxQueueSize = 4

openW :: Chunk
openW = XML $ Open "w" ""

closeW :: Chunk
closeW = XML $ Close "w"

isClose :: Text -> Node -> Bool
isClose expectedName (Close {name}) = name == expectedName
isClose _ _ = False

isOpen :: Text -> Node -> Bool
isOpen expectedName (Open {name}) = name == expectedName
isOpen _ _ = False

pushChunk :: TextChunk -> Chunks -> Reader WordSets Chunks
pushChunk (Separator "'") = glueApostrophe
pushChunk (Separator " ") = return
pushChunk chunk = return . (<> [openW, TextChunk chunk, closeW])

glueApostrophe :: Chunks -> Reader WordSets Chunks
glueApostrophe zipper = do
  previousWord <- findPreviousWord zipper
  case previousWord of
    Nothing -> return $ Zipper.insert (TextChunk $ Separator "'") zipper
    Just z -> return $ expandWord z

findPreviousWord :: Chunks -> Reader WordSets (Maybe Chunks)
findPreviousWord (Zipper [] _) = return Nothing
findPreviousWord zipper@(Zipper ((XML node):_) _) = do
  tagsAdmissibleUnderW <- asks wChildren
  if maybe True (`elem` tagsAdmissibleUnderW) $ getTag node
  then findPreviousWord $ Zipper.back zipper
  else return (if isClose "w" node then Just zipper else Nothing)
findPreviousWord _ = return Nothing

getTag :: Node -> Maybe String
getTag n@(Open {}) = Just . Text.unpack $ name n
getTag n@(Close {}) = Just . Text.unpack $ name n
getTag n@(Autoclosing {}) = Just . Text.unpack $ name n
getTag _ = Nothing

expandWord :: Chunks -> Chunks
expandWord _ = undefined

matching :: Node -> Chunks -> Maybe Chunks
matching (Open {name}) zipper = moveWhile forward foundClosing (zipper, [])
  where
    foundClosing (Zipper _ (XML node:_)) [] = isClose name node
    foundClosing _ _ = False
matching (Close {name}) zipper = moveWhile backward foundOpening (zipper, [])
  where
    foundOpening (Zipper _ (XML node:_)) [] = isOpen name node
    foundOpening _ _ = False
matching _ zipper = Just zipper

moveWhile :: ((Chunks, a) -> Maybe (Chunks, a)) -> (Chunks -> a -> Bool) -> (Chunks, a) -> Maybe Chunks
moveWhile move predicate (zipper, accumulator)
  | predicate zipper accumulator = return zipper
  | otherwise = move (zipper, accumulator) >>= moveWhile move predicate

backward :: (Chunks, [Text]) -> Maybe (Chunks, [Text])
backward (Zipper [] _, _) = Nothing
backward (zipper@(Zipper ((XML (Close {name})):_) _), tagStack) =
  Just (Zipper.back zipper, name:tagStack)
backward ((Zipper ((XML (Open {})):_) _), []) = Nothing
backward (zipper@(Zipper ((XML (Open {name})):_) _), (tag:stack))
  | name == tag = Just (Zipper.back zipper, stack)
  | otherwise = Nothing
backward (zipper, tagStack) = Just (Zipper.back zipper, tagStack)

forward :: (Chunks, [Text]) -> Maybe (Chunks, [Text])
forward (Zipper _ [], _) = Nothing
forward (zipper@(Zipper _ ((XML (Open {name})):_)), tagStack) =
  Just (Zipper.forth zipper, name:tagStack)
forward ((Zipper _ ((XML (Close {})):_)), []) = Nothing
forward (zipper@(Zipper _ ((XML (Close {name})):_)), (tag:stack))
  | name == tag = Just (Zipper.forth zipper, stack)
  | otherwise = Nothing
forward (zipper, tagStack) = Just (Zipper.forth zipper, tagStack)

markupName :: String -> String
markupName ('/':s) = s
markupName s = s

tokenize :: WordSets -> Strategy Chunks
tokenize parameters =
  Strategy $ \(Occurrence {node}) ->
    case node of
      Text content ->
        fmap $ pushChunks (Chunk.stream =<< lines (Text.unpack content))
      _ -> fmap (Zipper.insert (XML node) . zipDown)
  where
    pushAtEnd chunk = pushChunk chunk . zipDown
    pushChunks chunks zipper =
      runReader (foldl (>>=) (return zipper) (pushAtEnd <$> chunks)) parameters

main :: IO ()
main = do
  parameters <- Arguments.parse
  result <- apply (tokenize parameters) . Text.pack <$> getContents
  mapM_ outputChunk $ dump result
  where
    showChunk (XML node) = Text.unpack $ printNode node
    showChunk (TextChunk chunk) = Chunk.toString chunk
    outputChunk = putStrLn . showChunk
