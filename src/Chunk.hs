module Chunk (
      Chunk(..)
    , TextChunk(..)
    , stream
    , toString
  ) where

import Text.Regex (matchRegexAll, mkRegex)
import XML.Filter (Node(..))

{-
 - Chunks : a unit small enough to separate «words» (but often too small and that will need glueing)
 - The following are chunks
 -  * whole XML markups (either closing, opening, autoclosing, etc.)
 -  * XML entities in raw text
 -  * separators (individually)
 -  * blocks of texts between the above
 -}
data TextChunk = Entity String | Separator String | Raw String
data Chunk = TextChunk TextChunk | XML Node

toString :: TextChunk -> String
toString (Entity s) = s
toString (Separator s) = s
toString (Raw s) = s

-- |[][]|-|&(?:#[0-9a-fA-F]+|[a-zA-Z]+);

separators :: [Char]
separators = " ,:.-'?!()\"«»[]"

xmlEntity :: String -> Maybe (TextChunk, String)
xmlEntity =
  fmap keep . matchRegexAll (mkRegex "^(&(#x?[a-fA-F0-9]+|[a-zA-Z]+);)")
  where
    keep (_, matched, rest, _) = (Entity matched, rest)

next :: String -> (TextChunk, String)
next "" = error "Calling next on empty string"
next s@(c:rest)
  | c `elem` separators = (Separator [c], rest)
  | otherwise = case xmlEntity s of
      Just pair -> pair
      Nothing ->
        let (raw, end) = break (`elem` ('&':separators)) s in
        (Raw raw, end)

stream :: String -> [TextChunk]
stream "" = []
stream s = let (chunk, left) = next s in chunk:(stream left)
