{-# LANGUAGE NamedFieldPuns #-}
module Arguments (
      Parameters(..)
    , WordSets
    , parse
  ) where

import Control.Applicative ((<**>))
import Data.Monoid ((<>))
import Data.Set (Set)
import qualified Data.Set as Set (fromList)
import Options.Applicative (
      Parser, execParser, header, help, helper, info, long, metavar, short
    , strOption
  )

data Parameters a = Parameters {
      knownWords :: a
    , wChildren :: a
  }

type Files = Parameters FilePath
type WordSets = Parameters (Set String)

paths :: Parser Files
paths = Parameters
  <$> strOption (
        long "known words"
      <> short 'k'
      <> metavar "PATH"
      <> help "The path to a file containing a dictionary of all words containing a separator"
    )
  <*> strOption (
        long "wChildren"
      <> short 'w'
      <> metavar "PATH"
      <> help "The path to a file containing a list of all XML markup allowed under <w>"
    )

open :: Files -> IO WordSets
open (Parameters {knownWords, wChildren}) =
  Parameters <$> importSet knownWords <*> importSet wChildren
  where
    importSet = fmap (Set.fromList . lines) . readFile

parse :: IO WordSets
parse =
  execParser (info (paths <**> helper) (header "A tokenizer for Presto"))
  >>= open
