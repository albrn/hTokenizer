{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE TypeFamilies #-}
module Zipper (
      Zipper(..)
    , back
    , close
    , current
    , drop
    , edit
    , empty
    , forth
    , insert
    , index
    , isAtBegining
    , isAtEnd
    , mirror
    , move
    , next
    , open
    , pop
    , previous
    , replace
    , scrollNext
    , scrollPrevious
    , sub
    , take
    , zipDown
    , zipUp
  ) where

import Data.List (findIndex)
import GHC.Exts (IsList(..))
import Prelude hiding (drop, length, take)
import qualified Prelude (drop, take)

data Zipper a = Zipper {
      left :: [a]
    , right :: [a]
  } deriving Show

instance Foldable Zipper where
  foldr f z (Zipper left right) = foldl (flip f) (foldr f z right) left

instance Monoid (Zipper a) where
  mempty = Zipper [] []
  mappend zipperA zipperB = Zipper (left $ zipDown zipperA) (right $ zipUp zipperB)

instance IsList (Zipper a) where
  type Item (Zipper a) = a
  fromList = open
  toList = close

open :: [a] -> Zipper a
open l = Zipper {left = [], right = l}

close :: Zipper a -> [a]
close = right . zipUp

empty :: Zipper a
empty = open []

insert :: a -> Zipper a -> Zipper a
insert a (Zipper left right) = Zipper left (a:right)

current :: Zipper a -> a
current (Zipper _ []) = errorWithoutStackTrace "no element focused"
current (Zipper _ (a:_)) = a

pop :: Zipper a -> (a, Zipper a)
pop (Zipper [] []) = errorWithoutStackTrace "empty zipper"
pop (Zipper (a:as) []) = (a, Zipper as [])
pop (Zipper left (a:as)) = (a, Zipper left as)

drop :: Int -> Zipper a -> Zipper a
drop n (Zipper left right) = Zipper left (Prelude.drop n right)

replace :: a -> Zipper a -> Zipper a
replace a = insert a . drop 1

edit :: (a -> a) -> Zipper a -> Zipper a
edit f zipper = let (a, z) = pop zipper in insert (f a) z

take :: Int -> Zipper a -> Zipper a
take n (Zipper _ right) = Zipper [] (Prelude.take n right)

zipUp :: Zipper a -> Zipper a
zipUp (Zipper left right) = Zipper [] (reverse left ++ right)

zipDown :: Zipper a -> Zipper a
zipDown (Zipper left right) = Zipper (reverse right ++ left) []

mirror :: Zipper a -> Zipper a
mirror (Zipper left right) = Zipper right left

forth :: Zipper a -> Zipper a
forth (Zipper _ []) = errorWithoutStackTrace "at the end"
forth (Zipper left (a:as)) = Zipper (a:left) as

back :: Zipper a -> Zipper a
back (Zipper [] _) = errorWithoutStackTrace "at the begining"
back (Zipper (a:as) right) = Zipper as (a:right)

index :: Int -> Zipper a -> a
index n (Zipper left right)
  | n >= 0 = right !! n
  | otherwise = left !! ((-1) - n)

move :: Int -> Zipper a -> Zipper a
move n (Zipper left right)
  | n >= 0 =
    let (shifted, after) = splitAt n right in
    Zipper (reverse shifted ++ left) after
  | otherwise =
    let (shifted, before) = splitAt (-n) left in
    Zipper before (reverse shifted ++ right)

sub :: (Int, Int) -> Zipper a -> Zipper a
sub (from, length) (Zipper left right)
  | from >= 0 = Zipper [] (Prelude.take length $ Prelude.drop from right)
  | otherwise =
    let (under, above) = (min length (-from), from + length) in
    Zipper (Prelude.take under $ Prelude.drop (-above) left) (Prelude.take above right)

isAtBegining :: Zipper a -> Bool
isAtBegining (Zipper [] _) = True
isAtBegining _ = False

isAtEnd :: Zipper a -> Bool
isAtEnd (Zipper _ []) = True
isAtEnd _ = False

next :: (a -> Bool) -> Zipper a -> Maybe Int
next predicate (Zipper _ right) = findIndex predicate right

previous :: (a -> Bool) -> Zipper a -> Maybe Int
previous predicate (Zipper left _) = ((-1) -) <$> findIndex predicate left

scrollNext :: (a -> Bool) -> Zipper a -> Maybe (Zipper a)
scrollNext predicate zipper = (`move` zipper) <$> next predicate zipper

scrollPrevious :: (a -> Bool) -> Zipper a -> Maybe (Zipper a)
scrollPrevious predicate zipper = (`move` zipper) <$> previous predicate zipper
